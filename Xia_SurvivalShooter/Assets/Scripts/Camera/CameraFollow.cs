﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public GameController gC;
    public Transform day;
    public Transform night;
    public float smoothing = 5f;

    Transform target;
    Vector3 offset;

    void Start()
    {
        if (gC.isNight)
        {
            target = night;
        }
        else
        {
            target = day;
        }
        offset = transform.position - target.position;
    }

    private void Update()
    {
        if (gC.isNight)
        {
            target = night;
        }
        else
        {
            target = day;
        }
    }

    private void FixedUpdate()
    {
        Vector3 targetCamPos = target.position + offset;
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
    }
}
