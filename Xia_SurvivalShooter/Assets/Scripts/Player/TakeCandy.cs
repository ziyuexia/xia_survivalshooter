﻿using UnityEngine;
using System.Collections;

public class TakeCandy : MonoBehaviour
{
    GameObject bear;
    bool playerInRange;

    public bool given;


    void Awake()
    {
        bear = GameObject.FindGameObjectWithTag("Give");
        given = false;
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == bear)
        {
            playerInRange = true;
        }
    }


    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == bear)
        {
            playerInRange = false;
        }
    }


    void Update()
    {
        if (GameController.gC.isNight)
        {
            Destroy(gameObject);
        }
        else
        {
            if (playerInRange && !given)
            {
                GiveCandies();
            }
        }
    }


    void GiveCandies()
    {
        given = true;
        GameController.gC.energy += ScoreController.sC.energy;
        print("give Candy");
    }
}
