﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;

    Vector3 movement;
    Animator anim;
    Rigidbody playerRigidbody;
    PlayerHealth playerHealth;
    int floorMask;
    float camRayLength = 100f;
    float jumpforce = 6f;
    public bool onGround =false;
    public Transform groundCheck;
    float timer = 3f;
    bool onFloor;

    void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
        playerHealth = GetComponent<PlayerHealth>();
        OnFloor();
    }

    private void Update()
    {
        Jump();

        onGround = Physics.Raycast(groundCheck.position, Vector3.down, 0.1f);

        OnFloor();
        Punishment();
 
    }

    private void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        Move(h, v);
        Turning();
        Animating(h, v);
    }

    void Move(float h, float v)
    {
        movement.Set(h, 0f, v);
        movement = movement.normalized * speed * Time.deltaTime;
        playerRigidbody.MovePosition(transform.position + movement);
    }

    void Jump()
    {
        if (onGround && Input.GetKeyDown(KeyCode.Space))
        {
            playerRigidbody.AddForce(Vector3.up * jumpforce, ForceMode.Impulse);
        }
    }

    void Turning()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit floorHit;
        if(Physics.Raycast (camRay, out floorHit, camRayLength, floorMask))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            playerRigidbody.MoveRotation(newRotation);
        }
    }

    void Animating(float h, float v)
    {
        bool walking = h != 0f || v != 0f;
        anim.SetBool("IsWalking", walking);
        anim.SetBool("IsJumping", Input.GetKeyDown(KeyCode.Space));
    }

    void OnFloor()
    {
        if (playerRigidbody.transform.position.y > 0.05)
        {
            onFloor = false;
        }
        else
        {
            onFloor = true;
            timer = 2f;
        }
    }

    void Punishment()
    {
        if (!onFloor && timer <= 0)
        {
            playerHealth.TakeDamage(10);
            timer = 2f;
        }

        timer -= Time.deltaTime;
    }
}
