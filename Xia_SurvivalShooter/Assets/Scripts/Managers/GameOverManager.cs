﻿using UnityEngine;

public class GameOverManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public GameObject player;


    Animator anim;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        if (player.activeSelf)
        {
            //print("player active");
            if (playerHealth.currentHealth <= 0)
            {
                //print("Player die!");
                anim.SetTrigger("GameOver");
            }
        }
        else
        {
            //print("player diactivated");
        }
    }
}
