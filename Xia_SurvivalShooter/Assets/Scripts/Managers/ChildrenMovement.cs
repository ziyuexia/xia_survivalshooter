﻿using UnityEngine;

public class ChildrenMovement : MonoBehaviour
{
    Animator anim;
    TakeCandy tC;

    float speed = 3f;
    float leaveSpeed = 8f;
    int targetIndex;
    bool walking;
    Transform[] targets;
    Transform leavePoint;
    UnityEngine.AI.NavMeshAgent nav;

    void Start()
    {

        targets = GameController.gC.targetPoints;
        leavePoint = GameController.gC.leavePoint;
        anim = GetComponent<Animator>();
        tC = GetComponent<TakeCandy>();
        targetIndex = Random.Range(0, targets.Length);
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        walking = false;
    }

    void Update()
    {
        if (tC.given)
        {
            Leave();
        }
        else
        {
            Wander();
        }

        IsWalking();
    }

    void IsWalking()
    {
        if (Vector3.Distance(transform.position, targets[targetIndex].position) > 1f)
        {
            walking = true;
        }
        else
        {
            walking = false;
        }

        anim.SetBool("IsWalking", walking);

    }

    void Wander()
    {
        if (!walking)
        {
            targetIndex = Random.Range(0, targets.Length);
        }
        else
        {
            nav.speed = speed;
            nav.SetDestination(targets[targetIndex].position);
        }
    }

    void Leave()
    {
        if (Vector3.Distance(transform.position, leavePoint.position) > 1f)
        {
            walking = true;
            nav.speed = leaveSpeed;
            nav.SetDestination(leavePoint.position);
        }
        else
        {
            walking = false;
            Destroy(gameObject, 1f);
        }
        anim.SetBool("IsWalking", walking);

    }
}
