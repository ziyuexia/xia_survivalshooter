﻿using UnityEngine;

public class ChildrenManager : MonoBehaviour
{
    public GameController gC;
    public GameObject children;
    public float spawnTime = 3f;

    Transform[] spawnPoints;


    void Start()
    {
        spawnPoints = gC.targetPoints;
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }


    void Spawn()
    {
        if (!gC.isNight)
        {
            int spawnPointIndex = Random.Range(0, spawnPoints.Length);

            Instantiate(children, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
        }
    }
}
