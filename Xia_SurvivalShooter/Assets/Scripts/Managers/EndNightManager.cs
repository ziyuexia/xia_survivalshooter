﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndNightManager : MonoBehaviour {
    public bool clipBegin;
    public GameObject bGM;

    AudioSource alarmClip;
    AudioSource aS;
    Animator anim;
    Canvas canvas;

    void Awake()
    {
        anim = GetComponent<Animator>();
        aS = bGM.GetComponent<AudioSource>();
        canvas = GetComponent<Canvas>();
        alarmClip = GetComponent<AudioSource>();
        clipBegin = false;
        canvas.enabled = false;
    }


    void Update()
    {
        if (Mathf.FloorToInt(GameController.gC.clock) == 10f)
        {
            canvas.enabled = true;
            alarmClip.Play();
            print("End night!");
            clipBegin = true;
            anim.SetTrigger("EndNight");
            aS.volume = 0;
        }
    }
}
