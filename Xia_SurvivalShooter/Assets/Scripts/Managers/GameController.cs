﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static GameController gC;

    public Text scoreText;
    public Text currencyText;
    public Text countDownText;

    public Light lights;

    public bool isNight;
    public GameObject player;
    public GameObject bear;

    public int energy;

    public Transform[] targetPoints;
    public Transform leavePoint;
    public Slider bulletSlider;

    public GameObject upgradeCanvas;
    public bool endNight;

    public float clock;
    public float countDown;
    float min;
    float sec;
    float timePassed = 0f;








    // Use this for initialization
    void Awake () {
        if (gC != null)
        {
            print("GC WASN'T NULL! DESTROY!");
            Destroy(gC.gameObject);
        }
        print("SET TO NEW SINGLETON!");
        gC = this;
        //GameObject.DontDestroyOnLoad(gC.gameObject);

        isNight = false;
        endNight = false;


        //scoreText.text = "Score: work" + ScoreController.sC.score;

        countDown = ScoreController.sC.countDownTime;
        clock = ScoreController.sC.nightTime;


    }
	
	// Update is called once per frame
	void Update () {
        //print("Sc.score: " + sC.score);
        scoreText.text = "Currency: " + ScoreController.sC.score;
        currencyText.text = "Currency: " + ScoreController.sC.score;
        bulletSlider.value = energy;

        Countdown();
        CountDownText();
        SwitchLight();
        SwitchPlayer();
    }

    void Countdown()
    {
        if (isNight)
        {
            if (clock <= 0f)
            {
                endNight = true;
                isNight = false;
                Pause();
                countDown = ScoreController.sC.countDownTime; 
            }
            else
            {
                clock -= Time.deltaTime;
                timePassed += Time.deltaTime;
            }
            //print(clock);
        }

        else
        {
            if (countDown <= 0f)
            {
                isNight = true;
                clock = ScoreController.sC.nightTime;
            }
            else
            {
                countDown -= Time.deltaTime;
            }
        }
    }

    void CountDownText()
    {
        if(isNight)
        {
            if (clock == ScoreController.sC.nightTime)
            {
                countDownText.text = "12:00 AM";
            }
            else
            {
                //print("timePassed:" + Mathf.FloorToInt(timePassed));
                if (Mathf.FloorToInt(timePassed / ScoreController.sC.anHour) > 0)
                {
                    float hour = 0 + (Mathf.FloorToInt(timePassed / ScoreController.sC.anHour));
                    countDownText.text = hour + ":00 AM";
                }
            }
        }
        else
        {
            min = Mathf.FloorToInt(countDown / 60);
            sec = Mathf.FloorToInt(countDown % 60);
            if (sec < 10)
            {
                countDownText.text = min + " : 0" + sec;
            }
            else
            {
                countDownText.text = min + " : " + sec;
            }
        }

    }

    void SwitchLight()
    {
        if (isNight)
        {
            lights.color = new Color(0, 4, 53, 255);
            lights.intensity = 0.01f;
        }
        else
        {
            //light.color = Color.yellow;
           lights.color = new Color(255, 194, 0, 50);
           lights.intensity = 0.005f;
            
        }
    }


    void SwitchPlayer()
    {
        if (isNight)
        {
            player.SetActive(true);
            bear.SetActive(false);
        }
        else
        {
            player.SetActive(false);
            bear.SetActive(true);
        }
    }

    void Pause()
    {
        print("Paused");
        Time.timeScale = (Time.timeScale == 0) ? 1 : 0;
        //   Lowpass();

    }
}
