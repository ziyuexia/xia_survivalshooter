﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UpgradeManager : MonoBehaviour {

    public AudioClip trade;

    AudioSource aS;

	// Use this for initialization
	void Start () {
        aS = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ExtendDay()
    {
        if (ScoreController.sC.score >= 100)
        {
            TradeSound();
            ScoreController.sC.countDownTime += 5f;
            ScoreController.sC.score -= 100;
        }
    }

    public void Satisfaction()
    {
        if (ScoreController.sC.score >= 150)
        {
            TradeSound();
            ScoreController.sC.energy += 5;
            ScoreController.sC.score -= 150;
        }
    }

    public void HealthBoost()
    {
        if (ScoreController.sC.score >= 200)
        {
            TradeSound();
            ScoreController.sC.playerHealth += 5;
            ScoreController.sC.score -= 200;
        }
    }

    public void StartNew()
    {
        Time.timeScale = (Time.timeScale == 0) ? 1 : 0;
        ScoreController.sC.NewDay();
        SceneManager.LoadScene("Level 01");
    }

    public void StartOver()
    {
        Time.timeScale = (Time.timeScale == 0) ? 1 : 0;
        ScoreController.sC.Reset();
        SceneManager.LoadScene("Level 01");
    }

    void TradeSound()
    {
        aS.clip = trade;
        aS.Play();
    }
}
