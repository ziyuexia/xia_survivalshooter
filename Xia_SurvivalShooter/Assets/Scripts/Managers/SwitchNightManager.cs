﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchNightManager : MonoBehaviour {
    Animator anim;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        if (Mathf.FloorToInt(GameController.gC.countDown) == 1f)
        {
            //print("night!");
            anim.SetTrigger("SwitchNight");
        }
    }
}
