﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreController : MonoBehaviour {
    public static ScoreController sC;

    public int score;
    public int playerHealth;
    public int energy;

    public float nightTime;
    public float countDownTime;
    public float anHour;

    // Use this for initialization
    void Awake()
    {
        if (sC != null)
        {
            //print("GC WASN'T NULL! DESTROY!");
            Destroy(gameObject);
            return;
        }
        //print("SET TO NEW SINGLETON!");
        sC = this;
        //score = sC.score;

        ScoreController.DontDestroyOnLoad(sC.gameObject);
        anHour = 30f;
        nightTime = 6 * anHour;
        countDownTime = 60f;
        playerHealth = 100;
        energy = 10;
    }

    // Update is called once per frame
    void Update () {
        //print("MY SCORE: " + score + " SC.SCORE: " + sC.score);
	}

    public void Reset()
    {
        score = 0;
        anHour = 30f;
        nightTime = 6 * anHour;
        countDownTime = 60f;
        playerHealth = 100;
        energy = 10;
    }

    public void NewDay()
    {
        anHour += 5f;
    }
}
