﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimeTracker : MonoBehaviour {

    float bedTime;
    Text text;
    float min;
    float sec;

    // Use this for initialization
    void Start () {
        bedTime = 10f;
        text = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        Countdown();
	}

    void Countdown()
    {
        if (bedTime <= 0f)
        {

        }
        else
        {
            Clock();
            bedTime -= Time.deltaTime;
        }
    }

    void StartNight()
    {
        SceneManager.LoadScene("Level 01");
    }

    void Clock()
    {
         min = Mathf.FloorToInt(bedTime/60);
         sec = Mathf.FloorToInt(bedTime%60);
        if(sec < 10)
        {
            text.text = min + " : 0" + sec;
        }
        else
        {
           text.text = min + " : " + sec;
        }
    }
}
